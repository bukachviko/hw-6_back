import express from "express";
export const errorHandler = (err: any, req: express.Request, res: express.Response) => {
    console.error('Error:', err);
    res.status(500).send('Internal server error')
}

export default errorHandler;
import { Request, Response } from "express";
import { FileDB} from "../services/database.service";
import {errorHandler} from "./baseController";

export const FILEDB = 'fileDB.json';
export const getAllNewsPosts = async (req: Request, res: Response) => {
        try {
            const dbServiceInstance = await FileDB.getTable(FILEDB);
            const tableData = await dbServiceInstance.getAll();
            res.send(tableData)
        } catch (err) {
            console.log('Error:', err);
            errorHandler(err, req,res);
        }
    };

export const getNewsPostById = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
        try {
            const dbServiceInstance = await FileDB.getTable(FILEDB);
            const tableData = await dbServiceInstance.getById(newsId);
            if (!tableData) {
                res.status(404).send('News not found');
                return;
            }
            res.send(tableData);
        } catch (err) {
            console.log('Error:', err);
            errorHandler(err, req,res);
        }
    };

export const createNewsPost = async (req: Request, res: Response) => {
    const formData = req.body;
    try {
        const dbServiceInstance = await FileDB.getTable(FILEDB);
        const tableData = await dbServiceInstance.create(formData)
        res.send(tableData)
    } catch (err) {
        console.log('Error', err);
        errorHandler(err, req,res);
    }
};

export const updateNewsPost = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
    const formData = req.body;
    try {
        const dbServiceInstance = await FileDB.getTable(FILEDB);
        const tableData = await dbServiceInstance.update(newsId, formData);

        if (!tableData) {
            res.status(404).send('News not found');
            return;
        }
        res.send(tableData)
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req,res);
    }
};

export const deleteNewsPost = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
    try {
        const dbServiceInstance = await FileDB.getTable(FILEDB);
        const deletedNews = await dbServiceInstance.delete(newsId);
        if (!deletedNews) {
            res.status(404).send('News not found');
            return;
        }
        res.status(200).send('News deleted successfully');
        res.send()
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req,res);
    }
};

import express from 'express';
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from 'dotenv';
import {createNewsPost, deleteNewsPost, getAllNewsPosts, getNewsPostById, updateNewsPost} from './controllers/mainController';

dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use(cors({
    origin: 'http://localhost:3000'
}));

const PORT = process.env.PORT  ? parseInt(process.env.PORT) : 8000;
const HOST = process.env.HOST || 'localhost';

app.get('/api/newsposts', getAllNewsPosts);
app.get('/api/newsposts/:id',getNewsPostById);
app.post('/api/newsposts', createNewsPost);
app.put('/api/newsposts/:id',updateNewsPost);
app.delete('/api/newsposts/:id', deleteNewsPost);

app.listen(PORT, HOST, () => {
    console.log(`Server is running on http://${HOST}:${PORT}`);
})

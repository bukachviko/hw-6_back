export const NewsPostSchema: Record<string, string> = {
    id: 'Number',
    title: 'String',
    text: 'String',
    createDate: 'Date',
};
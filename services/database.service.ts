import fsPromise from 'fs/promises';
import path from 'path';
import { TableData } from "../interfaces/TableData.interface";
import { NewsPostSchema } from "../schemas/newPostSchema";
import { DbService } from "./DbService";


export class FileDB {
    static async RegistrSchema(fileName: string, schema: Record<string, string>): Promise<void> {
        try {
            const data: TableData = {
                table: [],
                id: 1,
                schema: schema,
            };

            await fsPromise.writeFile(fileName, JSON.stringify(data, null, 2));
            console.log(`Schema registered for ${fileName}`)
        } catch (err) {
            console.error(`Error creating file ${fileName}:`, err);
        }
    };

    static async getTable(tableName: string): Promise<DbService> {

        const filePath = path.join(__dirname, '..', 'db', tableName);
        let fileData: string | null = null;
        try {
            fileData = await fsPromise.readFile(filePath, 'utf-8');
        } catch (err) {
            await this.RegistrSchema(filePath, NewsPostSchema);
        }
        return new DbService(filePath, NewsPostSchema);
    };
}




import { NewsPost } from "../interfaces/NewsPost.interface";
import { TableData } from "../interfaces/TableData.interface";
import fsPromise from "fs/promises";

export class DbService {
    filePath: string;
    schema: Record<string, string>

    constructor(filePath: string, schema: Record<string, string>) {
        this.filePath = filePath;
        this.schema = schema;
    }

    private async readTable(): Promise<TableData> {
        const data = await fsPromise.readFile(this.filePath, 'utf-8');
        return JSON.parse(data)
    }

    async getAll():Promise<NewsPost[]> {
        const data = await this.readTable();
        return data.table;
    };

    async getById(id: number): Promise<NewsPost | null> {
        const data = await this.readTable();
        const item = data.table.find((item: { id: number }) => item.id === id);
        return item || null;
    };

    async create(newPostData: { title: string; text: string }): Promise<NewsPost | void> {
        try {
            const createDate = (new Date()).toString();
            const currentData = await this.readTable();
            const newPost: NewsPost = {id: currentData.id, ...newPostData, createDate};
            currentData.table.push(newPost);

            currentData.id++;

            await fsPromise.writeFile(this.filePath, JSON.stringify(currentData, null, 2));
            return newPost;

        } catch (err) {
            console.error(`Error creating object`, err);
            throw err
        }
    }

    async update(id: number, newData: { title?: string, text?: string }): Promise<NewsPost | void> {
        try {
            const data = await this.readTable();
            const index = data.table.findIndex(item => item.id === id);
            if (index === -1) return;
            const updatedItem = {...data.table[index], ...newData};
            data.table[index] = updatedItem;
            await fsPromise.writeFile(this.filePath, JSON.stringify(data, null, 2));

            return updatedItem;
        } catch (err) {
            console.error(`Error updating object`, err);
            throw err;
        }
    }

    async delete(id: number): Promise<number | void> {
        try {
            const data = await this.readTable();
            const index = data.table.findIndex((item => item.id === id))

            data.table.splice(index, 1);
            await fsPromise.writeFile(this.filePath, JSON.stringify(data, null, 2));
            return id;
        } catch (err) {
            console.log(`Error deleting object`, err);
            throw err;
        }
    }
}






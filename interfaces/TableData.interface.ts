import {NewsPost} from "./NewsPost.interface";

export interface TableData {
    id: number;
    table: NewsPost[];
    schema: Record<string, string>;
}

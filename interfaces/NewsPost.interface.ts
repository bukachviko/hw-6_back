export interface NewsPost {
    id: number,
    title: string,
    text: string;
    createDate: string;
}


